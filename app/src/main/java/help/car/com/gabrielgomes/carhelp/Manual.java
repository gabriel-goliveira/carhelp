package help.car.com.gabrielgomes.carhelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Manual extends AppCompatActivity {
    private ListView lista;
    private String servicos[] = {"Segurança", "Abrir/Fechar", "Visibilidade"};
    private ArrayAdapter adapter;

    //toolbar
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lista = (ListView) findViewById(R.id.lista);
        adapter = new ArrayAdapter<String>(
                Manual.this,
                R.layout.white_list2,
                R.id.textoLista,
                servicos
        );
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int codigoServico = position;
                String valor = lista.getItemAtPosition(codigoServico).toString();
                Intent it = new Intent(Manual.this, Tema.class);
                it.putExtra("servico", valor);
                startActivity(it);
            }


        });

    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.abrir: {
                Intent inte = new Intent(Manual.this, Abrir.class);
                startActivity(inte);
                break;
            }
            case R.id.buscar: {
                Toast.makeText(Manual.this, "Você está nessa página", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.mona: {
                Intent iit = new Intent(Manual.this, Mona.class);
                startActivity(iit);
                break;
            }
        }



        return super.onOptionsItemSelected(item);
    }

}
