package help.car.com.gabrielgomes.carhelp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Menu extends AppCompatActivity {
    //toolbar
    private Toolbar toolbar;

    //Componentes
    private TextView txt1;
    private Button bt1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt1 = findViewById(R.id.txt1);
        txt1.setText("O Car Help é um aplicativo que permite que o motorista tenha o controle do carro na palma da mão. \n \n" +
        "Para mais informações acesse:");
        bt1 = findViewById(R.id.bt1);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse("https://www.vw.com.br/pt.html");
                Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(webIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.abrir: {
                Intent inte = new Intent(Menu.this, Abrir.class);
                startActivity(inte);
                break;
            }
            case R.id.buscar: {
                Intent it = new Intent(Menu.this, Manual.class);
                startActivity(it);
                break;
            }
            case R.id.mona: {
                Intent iit = new Intent(Menu.this, Mona.class);
                startActivity(iit);
                break;
            }
        }




        return super.onOptionsItemSelected(item);
    }
}
