package help.car.com.gabrielgomes.carhelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class Tema extends AppCompatActivity {

    private TextView txt1;
    private TextView txt2;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tema);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt1 = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txt2);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            String opcaoEscolhida = extra.getString("servico");

        if(opcaoEscolhida.equals("Segurança")) {
            txt1.setText("Segurança");
            txt2.setText("Verificar regularmente a condição de todos os" +
                    "cintos de segurança. Em caso de avarias no ca-\n" +
                    "darço do cinto de segurança, ligações do cinto de" +
                    "segurança, enrolador automático do cinto de se-\n" +
                    "gurança ou fecho do cinto de segurança, o res-" +
                    "pectivo cinto deve ser substituído imediatamente\n" +
                    "por uma Concessionária Volkswagen → . Em-" +
                    "presas especializadas devem utilizar peças de re-\n" +
                    "posição corretas, compatíveis com o veículo, com" +
                    "a versão e com o ano-modelo. Para isso, a\n" +
                    "Volkswagen recomenda as Concessionárias" +
                    "Volkswagen.\n");


            }else if(opcaoEscolhida.equals("Abrir/Fechar")){
             txt1.setText("Abrir/Fechar");
             txt2.setText("Restabelecer a função de fechamento e" +
                     "abertura automática" +
                     "Se a bateria do veículo tiver sido desconectada\n" +
                     "ou descarregada com o vidro não fechado por" +
                     "completo ou, ainda, após alguns acionamentos" +
                     "da função limitador de força, a função de fecha-" +
                     "mento e abertura automática torna-se desativa-\n" +
                     "da e deve ser restabelecida:" +
                     "– Fechar todos os vidros.\n" +
                     "– Puxar a tecla do respectivo vidro para cima e\n" +
                     "manter nessa posição por pelo menos um se-" +
                     "gundo." +
                     "– Soltar a tecla e puxar novamente para cima e" +
                     "segurar. A função de fechamento e abertura\n" +
                     "automática está funcionando novamente." +
                     "Para as outras teclas com essa função desativa-" +
                     "da, repetir essa operação.");
        }else if(opcaoEscolhida.equals("Visibilidade")){
            txt1.setText("Visibilidade");
            txt2.setText("Para a segurança de condução é importante que" +
                    "o condutor ajuste corretamente os espelhos re-\n" +
                    "trovisores externos e o espelho retrovisor interno" +
                    "antes de iniciar a condução → ." +
                    "O condutor consegue observar o trânsito atrás de" +
                    "si pelos espelhos retrovisores externos e pelo es-\n" +
                    "pelho retrovisor interno e consegue adequar o" +
                    "seu comportamento de direção para o trânsito." +
                    "Não dá para ver tudo que está ao lado e atrás do veículo somente olhando para os espelhos retro-" +
                    "visores externos e para o espelho retrovisor in-" +
                    "terno. Estas áreas não visíveis são denominadas\n" +
                    "pontos cegos. No ponto cego podem haver ou-" +
                    "tros veículos, pedestres e objetos.");
        }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.abrir: {
                Intent inte = new Intent(Tema.this, Abrir.class);
                startActivity(inte);
                break;
            }
            case R.id.buscar: {
                Intent it = new Intent(Tema.this, Manual.class);
                startActivity(it);
                break;
            }
            case R.id.mona: {
                Intent iit = new Intent(Tema.this, Mona.class);
                startActivity(iit);
                break;
            }
        }



        return super.onOptionsItemSelected(item);
    }
}
