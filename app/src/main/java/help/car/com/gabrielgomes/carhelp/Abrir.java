package help.car.com.gabrielgomes.carhelp;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class Abrir extends AppCompatActivity {

    //toolbar
    private Toolbar toolbar;
    private ImageView img;
    private int identificador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abrir);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        img = findViewById(R.id.img);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identificador ==0){
                    identificador=1;
                    Drawable drawable= getResources().getDrawable(R.drawable.unlock6);
                    img.setImageDrawable(drawable);
                    Toast.makeText(Abrir.this,"Aberto", Toast.LENGTH_SHORT).show();
                }else if (identificador==1){
                    identificador=0;
                    Drawable drawable= getResources().getDrawable(R.drawable.lock);
                    img.setImageDrawable(drawable);
                    Toast.makeText(Abrir.this,"Fechado", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.abrir: {
                Toast.makeText(Abrir.this, "Você está nessa página", Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.buscar: {
                Intent it = new Intent(Abrir.this, Manual.class);
                startActivity(it);
                break;
            }

            case R.id.mona: {
                Intent iit = new Intent(Abrir.this, Mona.class);
                startActivity(iit);
                break;
            }
        }



        return super.onOptionsItemSelected(item);
    }
}
